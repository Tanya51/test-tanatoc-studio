$(document).ready(function () {
    $("#link-anchor").on("click", ".link-anchor", function (event) {
        //cancels standard click-through processing
        event.preventDefault();

        //take the side identifier from the href attribute
        var id = $(this).attr('href'),

            //learn the height from the beginning of the page to the block that the anchor refers to
            top = ($(id).offset().top) - 100;

        //animate the transition to the distance - top for 1500 ms
        $('body,html').animate({ scrollTop: top }, 1500);
    });
});