var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('sass', function () {
    return gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('css'));
});

gulp.task('scripts', function () {
    return gulp.src(['js/main/*.js'])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('watch' , function () {
    gulp.watch('scss/**/*.scss', ['sass']);
    gulp.watch('js/main/*.js', ['scripts']);
});